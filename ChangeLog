NOTE: This module is dead; the python bindings are now contained in Epiphany,
and the example extensions in Epiphany Extensions.

2005-05-02  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.override

	Add a wrapper to ephy_session_get_windows.
	Add a generic wrapper to build a PyList from a list (GList) of GObject.

	* examples/gtkcons.py

	Replace deprecated GtkEditable.append_text to GtkEditable.insert_text.

2005-04-18  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.defs

	Re-sync with epiphany API

2005-04-13  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Always check for -fno-strict-aliasing, not just in maintainer mode.

2005-03-31  Christian Persch  <chpe@cvs.gnome.org>

	* ephy-python/epiphany.defs:

	Make EphyStatusbar's tooltips accessible. Fixes bug #165477.

	* examples/sample-python-statusbar.py:

	Use the statusbar tooltips. Patch by Jean-François Rameau.

2005-03-31  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* loader/ephy-python-extension.c:

	Stupidity.
	
2005-03-31  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* loader/ephy-python-loader.c:
	* loader/loader.c:
	* loader/ephy-python-extension.c:
	* include/ephy-debug.h:

	Re-sync with Epiphany (ephy-debug.h).

2005-03-13  Christian Persch  <chpe@cvs.gnome.org>

	* examples/gtkcons.py:

	Don't use deprecated gtk.FALSE.

2005-03-07  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Version 0.2.0.

2005-02-27  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Post-release version bump.

2005-02-27  Christian Persch  <chpe@cvs.gnome.org>

	=== Release 0.1.2 ===

	* NEWS:

	Updated.

	* ephy-python/epiphany.defs:

	Re-sync with Epiphany.

2005-02-27  Adam Hooper  <adamh@cvs.gnome.org>

	* TODO:

	Remove the TODO which had to do with ephy_shell_get_default()

2005-02-12  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override
	* ephy-python/epiphany.defs

	Re-sync with epiphany API

	* README

	Describe how to re-sync the epiphany.defs file

2005-02-12  Xan Lopez <xan@gnome.org>

	* README:

	It's gnome-python/gnome-python, not gnome-python/pygnome.

2005-02-09  Christian Persch  <chpe@cvs.gnome.org>

	* Makefile.am:

	Dist po/LINGUAS.

2005-02-09  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:
	* po/LINGUAS:

	Get the list of languages from po/LINGUAS.

2005-02-08  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Post-release version bump.

2005-02-08  Christian Persch  <chpe@cvs.gnome.org>

	=== Release 0.1.1 ===

	* configure.ac:

2005-02-03  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:
	* examples/Makefile.am:

	Add samplelocation_DATA to EXTRA_DIST.

2005-01-29  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/Makefile.am:
	* examples/sample-python-location.py:
	* examples/sample-python-location.xml.in:

	Added sample extension which listens for location changes

2005-01-28  Crispin Flowerday  <gnome@flowerday.cx>

	* configure.ac: Add a message telling people to install
	epiphany-extensions if they don't have it already

2005-01-28  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/.cvsignore:

	Ignore .xml files

2005-01-28  Christian Persch  <chpe@cvs.gnome.org>

	* loader/ephy-python-loader.c: (ephy_python_loader_init):

	Call PySys_SetArgv(), otherwise it crashes with python 2.4. Fixes bug
	#165508, patch by Gustavo Carneiro.

2005-01-27  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:

	Remove as much unnecessary code as possible. Implement a couple more
	keybindings (ctrl-l: clear, ctrl-p: previous, ctrl-n: next). Make
	bracket-matching function simpler and less strict (i.e., it'll pass
	invalid code to the interpreter, where it belongs).

2005-01-27  Adam Hooper  <adamh@cvs.gnome.org>

	* ephy-python/epiphany.defs:
	* ephy-python/epiphany.override:
	* examples/Makefile.am:
	* examples/sample-python-statusbar.py:
	* examples/sample-python-statusbar.xml.in:

	Wrap EphyStatusbar, and create a sample extension which uses it.

2005-01-28  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Direct bugs to our new bugzilla product.

2005-01-27  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/Makefile.am:
	* examples/sample-python-menu.py:
	* examples/sample-python-menu.xml.in:

	Added a sample for generating menu entries

2005-01-24  Christian Persch,,,  <chpe@cvs.gnome.org>

	* configure.ac:

	Post-release version bump.

2005-01-24  Christian Persch,,,  <chpe@cvs.gnome.org>

	=== Release 0.1 ===
	
	* Makefile.am:
	* configure.ac:
	* examples/Makefile.am:
	R examples/gtkcons.xml:
	A examples/gtkcons.xml.in:
	R examples/sample-python.xml:
	A examples/sample-python.xml.in:
	* po/POTFILES.skip:

2005-01-24  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Update Epiphany dependency to 1.5.5.

2005-01-23  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.defs:

	Resync with Epiphany (some changes in ephy-embed.h).

2005-01-22  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.defs:

	Add handling of EphyNode.
	Add handling of EphyBookmarks.
	Wrap ephy_node_get_children.

2005-01-17  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.defs:

	Remove ephy_node_get_property_time according to Epiphany's api.

2005-01-15  Jean-François Rameau  <jframeau@cvs.gnome.org>

	* ephy-python/epiphany.defs:
	* ephy-python/epiphany.override:

	Add handling of EphyCookie type.
	Add handling of ephy_cookie_manager_list_cookies.
	Change password manager's method names to get more comprehensive names.
	
2005-01-15  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:
	* loader/ephy-python-extension.c: (call_python_func):

	Remove ephy_shell hack (Python extensions can use
	ephy_shell_get_default() instead).

2005-01-15  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.defs:

	Add ephy_shell_get_default()

2005-01-15  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.defs
	* ephy-python/epiphany.override:

	Add handling of EphyPasswordInfo, and ephy_password_manager_list,
	also ignore ephy_shell_new and ephy_shell_error_quark

2005-01-14  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Set Epiphany dependency to >= 1.5.4.

2005-01-09 Jean-François Rameau <jframeau@cvs.gnome.org>

	* configure.ac:

	Add -fno-strict-aliasing to CFLAGS if the compiler supports it.  
	The Python API breaks some of the strict aliasing rules.

2005-01-09  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:

	Use gtk.ActionGroup.add_actions(), bug #162874 is fixed. In the
	process, the "sys:1: GtkWarning: gtk_container_foreach: assertion
	`GTK_IS_CONTAINER (container)' failed" warning on window close has
	disappeared; I don't know why, but I'm not complaining.

2005-01-07  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (unset_python_search_path):

	Use PySequence_DelSlice instead of DelItem

2005-01-06  Christian Persch  <chpe@cvs.gnome.org>

	* ephy-python/epiphany.override:

	Include ephy-lib-type-builtins.h.

2005-01-06  Christian Persch  <chpe@cvs.gnome.org>

	* Makefile.am:

	Remove tools/copy-template from EXTRA_DIST, it doesn't exist.

	* ephy-python/Makefile.am:

	Fix srcdir != builddir builds.

	* ephy-python/epiphany.override:

	Include type builtins.

	* ephy-python/epiphanymodule.c:

	Add prototype.

2005-01-05  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override
	* ephy-python/epiphany.defs:

	Leave the ephy_tab_for_embed() function in the .defs file, but
	just ignore it in the .override file.
	
2005-01-05  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Don't enable -Werror by default for now, since it doesn't compile.

2005-01-05  Adam Hooper  <adamh@cvs.gnome.org>

	* ephy-python/epiphany.defs:
	* ephy-python/epiphanymodule.c: (initepiphany):

	Remove ephy_tab_for_embed, make ephy_dialog_new_with_parent a
	constructor, and register constants (unfortunately, they have to all
	start with "EPHY_" to work properly, and they don't).

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* README:

	Update a bit.

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* ephy-python/epiphany.defs:
	* examples/gtkcons.py:
	* loader/ephy-python-extension.c: (call_python_func):

	Oh, I get it! ephy_shell inherits from embed_shell! (Only took me
	years of ephy hacking to spot this. Um.)

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:
	* loader/ephy-python-extension.c: (call_python_func):

	Add embed_shell

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:

	Improve UI marginally: make font monospace, and make Escape/Ctrl-W
	hide the window (it only closes when the EphyWindow parent closes).

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* TODO:

	Mention ephy_shell hack

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c:
	(ephy_python_extension_constructor):

	Reload the extension instead of keeping the old one.

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:
	* loader/ephy-python-extension.c:
	(ephy_python_extension_constructor), (call_python_func):

	Put ephy_shell into extension's module before calling any functions.

2005-01-05  Christian Persch  <chpe@cvs.gnome.org>

	* examples/gtkcons.py:

	Pack the text view in a SHADOW_IN scrolled window.

2005-01-05  Christian Persch  <chpe@cvs.gnome.org>

	* Makefile.am:
	* configure.ac:
	A examples/Makefile.am:

	Add Makefile.am in examples/ which installs the example extensions.

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:

	Don't use gtk.ActionGroup.add_actions(), it's buggy (Bug #162874)

2005-01-04  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (call_python_func),
	(impl_attach_tab), (impl_detach_tab), (impl_attach_window),
	(impl_detach_window):
	* loader/ephy-python-loader.c: (ephy_python_loader_finalize):

	Don't try to impose our refcounting on PyGTK: instead, just put a
	"PyGC_Collect" into the idle loop to detect when an EphyWindow hits 1
	refcount.

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* README:

	Put installation instructions in.

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.xml:
	* examples/sample-python.py:
	* examples/sample-python.xml:

	Added sample extension, changed "chpe@gnome.org" to "adamh@densi.com"

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c:

	Add newlines in The Huge Comment.

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* examples/gtkcons.py:
	* examples/gtkcons.xml:

	Added Python Console.

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override (_wrap_ephy_embed_event_get_property): 
	Add wrapper for ephy_embed_event_get_property, and don't
	export ephy_embed_event_get_dom_event

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override: Add wrappers
	for ephy_embed_single_get_font_list and
	ephy_embed_single_get_printer_list

2005-01-03  Christian Persch  <chpe@cvs.gnome.org>

	* ephy-python/Makefile.am:

	Add missing backslash.

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override
	* ephy-python/epiphany.defs: 

	Fix the shistory_get_nth wrapper, and ensure that python
	treats the interfaces as interfaces no objects.

2005-01-03  Christian Persch  <chpe@cvs.gnome.org>

	* ephy-python/Makefile.am:

	Make it work for srcdir != builddir builds.

	* loader/Makefile.am:
	* loader/ephy-python-extension.c:
	(ephy_python_extension_get_property),
	(ephy_python_extension_set_property):

	Use g_value_dup_string().

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.override (_wrap_ephy_window_get_tabs): 

	Wrap ephy_embed_event_get_coords()

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.defs:
	* ephy-python/epiphany.override: Update defs, so that the
	enums are exported, and only public API is exported.

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (get_quark), (incref), (decref),
	(impl_attach_tab), (impl_detach_tab), (impl_attach_window),
	(impl_detach_window):

	Use PyObject->ob_refcnt to handle multiple Python extensions.

2005-01-03  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (pygobject_new_unreffed),
	(get_quark), (call_python_func), (impl_attach_tab),
	(impl_detach_tab), (impl_attach_window), (impl_detach_window):

	Fix signals: create the PyGObject on attach_* and destroy it on
	detach. This will BREAK HORRIBLY if more than one Python extension is
	run at a time.

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* loader/ephy-python-extension.c (call_python_func):
	Don't complain every time a window or tab is created and destroyed
	if a python script fails to compile.

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/epiphany.defs
	* ephy-python/epiphany.override: Add specialized
	overrides for some functions that returns lists, or
	compound values

2005-01-03  Crispin Flowerday  <gnome@flowerday.cx>

	* ephy-python/Makefile.am
	* ephy-python/epiphany.override
	* ephy-python/epiphany.defs: Wrap most of the public API, not complete
	due to missing types for enums, and the need for manual wrappers
	for some functions

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* ephy-python/.cvsignore:
	* ephy-python/epiphany.c:
	* ephy-python/epiphany.defs:

	Remove epiphany.c -- it's automatically generated.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* Makefile.am:
	* configure.ac:

	Put ACLOCAL_AMFLAGS into Makefile.am -- that way "make" works even when
	configure.ac is updated.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* Makefile.am:
	* configure.ac:
	* ephy-python/Makefile.am:
	* ephy-python/epiphany.c: (_wrap_ephy_tab_new),
	(_wrap_ephy_tab_get_location), (_wrap_ephy_tab_get_title),
	(_wrap_ephy_window_new), (_wrap_ephy_window_find),
	(_wrap_ephy_window_load_url), (pyepiphany_register_classes):
	* ephy-python/epiphany.defs:
	* ephy-python/epiphany.override:
	* ephy-python/epiphanymodule.c: (initepiphany):

	Got some preliminary wrappers in. It's easy to add more....

	* loader/ephy-python-extension.c: (pygobject_new_unreffed),
	(call_python_func):

	Don't use pygobject_new(). It adds a Python reference to the GObject
	which can't be removed properly. Instead, copy over only the bits of
	pygobject_new() which are actually useful.

2005-01-03  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	ACLOCAL_AMFLAGS.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* AUTHORS:
	* HACKING:

	Updated to deal with pyphany, not epiphany-extensions

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (call_python_func):

	Added comment about refcounts.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (call_python_func):

	Decrement window/tab refcounts

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-loader.c: (ephy_python_loader_init):

	init_gobject () and init_pygtk ().

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c:
	(ephy_python_extension_class_init):

	g_param_spec_STRING

2005-01-03  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:
	* loader/Makefile.am:

	Add -l to PY_LIBS.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (set_python_search_path),
	(unset_python_search_path), (ephy_python_extension_constructor),
	(ephy_python_extension_class_init):

	Don't use setenv, use PySys_GetObject("path").

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* loader/Makefile.am:
	* loader/ephy-python-loader.c: (impl_get_object),
	(impl_release_object), (ephy_python_loader_finalize):

	Make it compile.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* include/Makefile.am:
	* include/ephy-file-helpers.h:
	* loader/ephy-python-extension.c: (ephy_python_extension_init):

	Make ephy-python-extension.c compile!

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* include/.cvsignore:
	* include/Makefile.am:
	* loader/.cvsignore:

	Add Makefile.am.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (ephy_python_extension_finalize),
	(ephy_python_extension_constructor), (call_python_func):

	Remove reference to self, and fix obvious errors.

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* Makefile.am:
	* configure.ac:
	* include/ephy-debug.h:
	* loader/ephy-python-extension.c:

	Copied from ephy.

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* loader/Makefile.am:

	Add python include dir (hardcoded for now).

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* loader/Makefile.am:

	Fix targets.

2005-01-02  Christian Persch  <chpe@cvs.gnome.org>

	* configure.ac:

	Define EXTENSIONS_DIR and LOADER_DIR in config.h.

	* loader/Makefile.am:
	* loader/ephy-python-extension.c:
	(ephy_python_extension_register_type):
	* loader/ephy-python-extension.h:
	* loader/ephy-python-loader.c: (impl_get_object),
	(impl_release_object), (ephy_python_loader_iface_init),
	(ephy_python_loader_init), (ephy_python_loader_finalize),
	(ephy_python_loader_class_init), (ephy_python_loader_get_type),
	(ephy_python_loader_register_type):
	* loader/ephy-python-loader.h:

	A python module loader.

	* loader/loader.c: (register_module):

	Register our types, and return the loader's type, not the extension's.

2005-01-02  Adam Hooper  <adamh@cvs.gnome.org>

	* loader/ephy-python-extension.c: (ephy_python_extension_get_type),
	(ephy_python_extension_register_type),
	(ephy_python_extension_finalize), (set_python_search_path),
	(ephy_python_extension_constructor),
	(ephy_python_extension_get_property),
	(ephy_python_extension_set_property),
	(ephy_python_extension_class_init), (ephy_python_extension_init),
	(call_python_func), (impl_attach_tab), (impl_detach_tab),
	(impl_attach_window), (impl_detach_window),
	(ephy_python_extension_iface_init):
	* loader/ephy-python-extension.h:

	Added untested (therefore most likely broken) ephy-python-extension.
