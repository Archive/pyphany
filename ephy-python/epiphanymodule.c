#include <pygobject.h>
 
void pyepiphany_register_classes (PyObject *d); 
void pyepiphany_add_constants (PyObject *module, const gchar *strip_prefix);

extern PyMethodDef pyepiphany_functions[];

DL_EXPORT(void) initepiphany(void);

DL_EXPORT(void)
initepiphany(void)
{
	PyObject *m, *d;

	init_pygobject ();

	m = Py_InitModule ("epiphany", pyepiphany_functions);
	d = PyModule_GetDict (m);

	pyepiphany_register_classes (d);
	pyepiphany_add_constants (m, "EPHY_");
}
